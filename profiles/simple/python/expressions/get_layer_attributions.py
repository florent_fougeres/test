from qgis.core import QgsLayoutItemMap, QgsProject, qgsfunction


@qgsfunction(args="auto", group="Custom")
def getLayerAttributions(value1, feature, parent, context):
    attributions = []
    delimiter = ", "

    # List map items of layout
    layout_name = context.variable("layout_name")
    project = QgsProject.instance()
    layoutManager = project.layoutManager()
    currentLayout = layoutManager.layoutByName(layout_name)

    # iterates over map items
    for i in currentLayout.items():
        if isinstance(i, QgsLayoutItemMap):
            for lyr in i.layersToRender():
                md = lyr.metadata()
                for e in md.rights():
                    # trim eventual spaces
                    e = e.strip()
                    attributions.append(
                        e
                    )  # attention, c'est une liste dans une liste. A éclater

    # removes duplicates
    attributions = list(set(attributions))

    citation = delimiter.join(attributions)

    return citation
